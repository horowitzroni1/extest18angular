import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpParams} from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class UsersService {

  url = environment.url;
  getUsers(){ 
    return this.http.get(this.url +'users');
  } 
  
  postUser(user){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('name',user.name).append('phone',user.phone);
    return this.http.post(this.url + 'users',params.toString(), options);     
  }

  deleteUser(id){
    return this.http.delete(this.url + 'users/'+id); 
  }

  updateUser(id,user){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('name',user.name).append('phone',user.phone);
    return this.http.put(this.url + 'users/'+id, params.toString(), options);      
  }

  getMessagesFire(){
    return this.db.list('/users').valueChanges();
  }
  
  constructor(private http:Http, private db:AngularFireDatabase) { }

}
