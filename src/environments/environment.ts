// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  url:'http://localhost/slim/',
  firebase:{
    apiKey: "AIzaSyAX0WaavmtiY40EF_wpB1o1yRyLmhhmsTA",
    authDomain: "messages-e2f57.firebaseapp.com",
    databaseURL: "https://messages-e2f57.firebaseio.com",
    projectId: "messages-e2f57",
    storageBucket: "messages-e2f57.appspot.com",
    messagingSenderId: "336171505130"
  },  
  production: false
};
